��    q      �      ,      ,  
   -     8     Q     f     |     �  
   �     �  	   �     �     �     �               8  !   W     y     �      �     �     �     	     #	     /	     ;	     K	     g	     l	     |	     �	     �	  "   �	     �	     �	     �	     �	     
     
     
     )
  
   0
     ;
     I
     `
     m
  
   {
  	   �
     �
  	   �
  5   �
     �
     �
     �
     �
                    1     A  a   G  	   �     �     �     �     �     �  	   �  	   �            	   )  	   3     =     E     L  	   Y     c     l     z  	   �     �     �     �     �     �     �  	   �     �     �     �  
          	   $  
   .  $   9     ^     e     l     |     �     �  9   �  !   �  )        I     Q     W     \     r     y     �     �  �  �     8     G     g     �     �     �     �     �     �       	        )     ?     Z  )   u  /   �     �  "   �  )     '   :  &   b     �     �     �     �     �  
   �               *     0  %   >     d     z     �  $   �  	   �     �     �     �     �       $        <     O     `  
   s  	   ~  	   �  O   �     �     �                       "   :     ]     w  c   ~     �     �     �     �     
     $     *     <     L  )   d     �     �     �     �     �     �     �     �     �          "  
   8     C     `     e  
   v     �     �     �     �     �     �     �     �  �   �     �  
   �     �     �  #   �  A     H   \  /   �  ,   �  
     	                  /     ;  
   K     V   % Comments %s css file cannot found %s file cannot found %s file cannot found. &larr; Older Comments (%s) function cannot found (optional) - Select icon set - 1 Comment 404 Error Page Sidebar 404 Page 404 Page Title <strong>%d</strong> Like <strong>%d</strong> Likes <strong>404</strong> Not Found <strong>About</strong> the Author <strong>Comments</strong> <strong>Comments</strong> (%s) <strong>Like</strong> this post! <strong>Related</strong> Posts <strong>Share</strong> the Post Add New Category Added Files All Filters All posts by %s An error has been occurred. Back Calendar Widget Cancel reply Close Comment Comment form template cannot found Comment navigation Comments Count Comments are closed. Continue reading this entry Copy Create Custom 404 Page Delete Delete All Deleted Files Display Related Posts? Done Editing Edit Category Edit Title Edit/Done Edit: %s Error 404 File has an invalid extension, it should be one of %s File is empty File is too large Forum Generate Import Leave a Reply Leave a Reply to %s Leave a comment Likes Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a> Move Down Move Up Name New Category Name Newer Comments &rarr; Next Next Page Next Post No comment yet. No files were uploaded. No repeat Not Found Page %s Pages: Post Comment Post Tags Previous Previous Page Previous Post Read More Read More Link Remove Required fields are marked %s Save Save Changes Saved Saving... Search Search Category Search Filter Search Key Select Page Set Name: Tag: Tags: The page you try to see cannot found Title: Upload Upload An Image View all % comments View all posts by %s We couldn't found any result. You must be <a href="%s">logged in</a> to post a comment. Your comment is awaiting approval Your email address will not be published. by %1$s clone edit http://wordpress.org/ import open the page remove reply Project-Id-Version: Envision v1.0.4
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-11-26 20:04+0200
PO-Revision-Date: Sun Sep 06 2015 12:07:15 GMT+0700 (SE Asia Standard Time)
Last-Translator: Nguyễn Thanh Hòa <uit.nth@gmail.com>
Language-Team: 
Language: Vietnamese
Plural-Forms: nplurals=1; plural=0
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-SourceCharset: UTF-8
X-Generator: Loco - https://localise.biz/
X-Poedit-KeywordsList: _:1;gettext:1;dgettext:2;ngettext:1,2;dngettext:2,3;__:1;_e:1;_c:1;_n:1,2;_n_noop:1,2;_nc:1,2;__ngettext:1,2;__ngettext_noop:1,2;_x:1,2c;_ex:1,2c;_nx:1,2,4c;_nx_noop:1,2,3c;_n_js:1,2;_nx_js:1,2,3c;esc_attr__:1;esc_html__:1;esc_attr_e:1;esc_html_e:1;esc_attr_x:1,2c;esc_html_x:1,2c;comments_number_link:2,3;t:1;st:1;trans:1;transChoice:1,2
X-Poedit-Basepath: ../
X-Textdomain-Support: yes
X-Poedit-SearchPath-0: ..
X-Loco-Target-Locale: vi_VN % bình luận @s tệp css không tìm thấy %s tệp không tìm thấy %s tập không tìm thấy
 &larr; Bình luận cũ (%s) không tìm thấy hàm (tùy chọn) - Chọn biểu tượng -
 1 Bình luận Lỗi trang 404  Trang 404 404 Tiêu đề trang <strong>%d</strong> Thích <strong>%d</strong> Thích <strong>404</strong> - Không tìm thấy <strong>Xem thông tin</strong> người viết <strong>Bình luận</strong> <strong>Bình luận</strong> (%s) <strong>Thích</strong> bài viết này! <strong>Bài viết</strong> liên quan <strong>Chia sẻ</strong> bài viết Thêm danh mục mới Các tệp đã thêm
 Lọc tất cả Tất cả bài viết của %s Đã xẩy ra lỗi
 Trở về Widgest Lịch
 Hủy trả lời Hủy Bình luận
 Không tìm thấy bình luận mẫu Chuyển bình luận Số bình luận Khóa bình luận Tiếp tục đọc bài viết này Sao chép Tạo:
 Tùy chỉnh trang 404 Xóa Xóa tất cả Các tệp đã xóa Hiển thị bài viết liên quan? Chỉnh sửa xong Sửa danh mục Sửa tiêu đề
 Sửa/Xong Sửa: %s Lỗi 404 Tập tin có phần mở rộng không hợp lệ, nó sẽ là một trong %s Tệp trống Tệp quá lớn Diễn đàn Tạo Đưa vào
 Để lại bình luận
 Để lại bình luận tới %s
 Để lại bình luận
 Thích Đăng nhập với <a href="%1$s"> % 2 $ s </a>.<a href="%3$s" title="Đăng xuất"> Thoát? </a> Xuống Lên Tên Tên danh mục mới Bình luận mới &rarrl Tới Trang tiếp theo Bài viết sau Không có bình luận Chưa có tệp nào được tải lên. Không lặp lại Không tìm thấy Trang %s Trang Đăng bình luận Thẻ bài viết Lùi
 Trang trước Bài viết trước Đọc tiếp
 Xem thêm liên kết Hủy bỏ Bắt buộc đánh dấu %s Lưu Lưu thay đổi Đã lưu
 Đang lưu
 Tìm kiếm
 Danh mục tìm kiếm Tìm & lọc
 Từ khóa tìm kiếm Chọn trang Đặt tên:
 Thẻ  Không tìm thấy trang bạn yêu cầu. 
Có thể bạn đã nhập sai đường dẫn hoặc bạn không có quyền truy cập trang này.
Vui lòng thử lại. Tiêu đề
 Tải lên Tải lên một bức ảnh Xem tất cả % bình luận Xem tất cả bài viết của %s Chúng tôi không thể tìm thấy bất kỳ kết quả nào. Bạn phải đăng nhập <a href="%s"> </a> để viết bình luận. Bình luận của bạn đang được duyệt Địa chỉ mail sẽ được bảo mật
 bởi %1$s bản sao sửa http://itbiboo.vn Đưa vào
 Mở trang web
 hủy bỏ bình luận 